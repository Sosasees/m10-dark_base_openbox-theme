# M10-Dark_Base Openbox theme

![theme preview from ``obconf``](obconf-preview.png)

M10-Dark_Base is a custom theme i made for [Openbox](https://en.wikipedia.org/wiki/Openbox) 3.
i made it as a modern-looking theme for Openbox since the pack-in themes look too old for me.

---

M10-Dark_Base Openbox Theme by (C) 2022 Sosasees

the [``themerc``](./openbox-3/themerc) is licensed under [GNU General Public License 3.0](./LICENSES/gpl-v3.txt) or later.
the images — [spritesheet](./spritesheet.png) and [XBM files](./openbox-3/) — are licensed under [Creative Commons 0 1.0 Universal](./LICENSES/cc0.txt).

## inspiration

### name inspiration

**M10** is short for **M**aterial Design & Windows **10**,
because this theme is inspired by Material Design
and looks a lot like Windows 10.

**Dark** means that this theme is a dark theme.

**Base** means that this is the base version of the theme because it uses 'pure grey' colors — colors without saturation.
by picking pure grey colors, i could focus only on if the lightness is right — because lightness is much more important than hue and saturation.

i might never make variations of this theme, but i still thought that making the name convention good for varitaions is a good idea.

### icon inspiration

icons are inspired by Google Material Design.
each icon is 34×34px large:
- the middle 20×20px is used for the icon itself
- the rest is padding so that i could fit the circle hover effect

## documentation used

[Openbox Wiki: Help: Themes](http://openbox.org/wiki/Help:Themes)

## tools used

- [OK Color Picker](https://ok-color-picker.netlify.app/)
  to pick the colors in okHSL space
- Terminal
  to edit the ``themerc`` text document

### for icon creation

1. [Pixelorama](https://orama-interactive.itch.io/pixelorama)
   to create the [spritesheet](spritesheet.png)
2. [XBM Viewer and Converter]
   to convert the sprites from the spritesheet to the XBM files
   that are used in the actual theme
3. [XBM Editor]
   to clean up the result from _XBM viewer and converter_
4. Terminal
   to create the final XBM files by pasting the result from _XBM editor_
